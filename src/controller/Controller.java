package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Client;
import model.Product;
import model.W_Order;
import presentation.AbstractWindow;
import presentation.ClientForm;
import presentation.ProductForm;
import presentation.W_OrderForm;

/**
 * Controller of the AbstractWindow<T>
 */

public class Controller<T> {
	/**
	 * Adds listeners to the 3 buttons (DELETE, UPDATE, ADD)
	 * @param window - reference to the corresponding window, containing the table of objects
	 */
	   public Controller(AbstractWindow<T> window) {
	        window.addDeleteListener(
		        	new ActionListener() {
		        		public void actionPerformed(ActionEvent e) {
		                	try {
		                		window.deleteByIndex(window.getTableIndex());
		                	} catch (Exception ex) {
		                		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
		                	}
		                }
		        	}
	        );
	        
	        window.addUpdateListener(
		        	new ActionListener() {
		        		@SuppressWarnings("unchecked")
						public void actionPerformed(ActionEvent e) {
		                	try {
		                		switch (window.getClass().getName()) {
	                			case "presentation.ClientWindow": new ClientForm((AbstractWindow<Client>) window,
	                														window.getFieldValuesByIndex(window.getTableIndex()));
	                							break;
	                			case "presentation.ProductWindow": new ProductForm((AbstractWindow<Product>) window,
																			window.getFieldValuesByIndex(window.getTableIndex()));
    											break;
	                			case "presentation.W_OrderWindow": new W_OrderForm((AbstractWindow<W_Order>) window,
																			window.getFieldValuesByIndex(window.getTableIndex()));
    											break;
	                			default: break;
	                		}
		                	} catch (Exception ex) {
		                		JOptionPane.showMessageDialog(window, ex.getMessage());
		                	}
		                }
		        	}
	        );
	        
	        window.addAddListener(
		        	new ActionListener() {
		        		@SuppressWarnings("unchecked")
						public void actionPerformed(ActionEvent e) {
		                	try {
		                		switch (window.getClass().getName()) {
		                			case "presentation.ClientWindow": new ClientForm((AbstractWindow<Client>) window);
		                							break;
		                			case "presentation.ProductWindow": new ProductForm((AbstractWindow<Product>) window);
        											break;
		                			case "presentation.W_OrderWindow": new W_OrderForm((AbstractWindow<W_Order>) window);
        											break;
		                			default: break;
		                		}
		                	} catch (Exception ex) {
		                		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
		                	}
		                }
		        	}
	        );
	   }
}
