package DAO;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import dataAccessLayer.ConnectionFactory;

/**
 * 
 * @author Attila
 * @param <T> It can be a database object entity
 * Must be extended by specifying type T and implementing buildObject() method
 * Contains the interactions with the database (find, findAll, insert, update)
 */

public abstract class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	/**
	 * Constructor, sets the type field to the actual generic type
	 */
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/**
	 * Creates a SELECT query
	 * @param field - name of the fields in WHERE clause 
	 * @return the SELECT query (String)
	 */
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		if (field!=null) sb.append(" WHERE " + field + " = ?");
		return sb.toString();
	}

	/**
	 * @return the INSERT query
	 */
	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" (");
		int countFields=0;
		for (Field field : type.getDeclaredFields()) {
			sb.append(field.getName());
			sb.append(", ");
			countFields++;
		}
		sb.delete(sb.length()-2, sb.length());
		sb.append(") VALUES (");
		for (int i=0; i<countFields; i++) {
			sb.append("?, ");
		}
		sb.delete(sb.length()-2, sb.length());
		sb.append(") ON DUPLICATE KEY UPDATE ");
		for (Field field : type.getDeclaredFields()) {
			sb.append(field.getName());
			sb.append(" = ?, ");
		}
		sb.delete(sb.length()-2, sb.length());
		return sb.toString();
	}
	
	/**
	 * @return the DELETE query
	 */
	private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" WHERE " + field + " = ?");
		return sb.toString();
	}

	/**
	 * Finds all rows from a database table
	 * @return a list of objects (type T)
	 */
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(null);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * @param id 
	 * @return
	 */
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	/**
	 * 
	 * @param field - name of the field in WHERE clause
	 * @param find - value of the field in WHERE clause
	 * @return a List of objects (type T) corresponding to the given condition (if the field is primary key -> a single row returned)
	 */
	public List<T> findByField(String field, Object find) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(field);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setObject(1, find);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Transform resultset of a query into List of objects
	 * @param resultSet
	 * @return
	 */
	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Abstract method, must be implemented
	 * <p>
	 * Builds the insert query by inserting specific values 
	 * @param statement - must be insert statement
	 * @param t - object (type T)
	 */
	public abstract void buildObject(PreparedStatement statement, T t);
	
	/**
	 * Inserts an object into the database table (table T)
	 * @param t - ObJect to be inserted
	 */
	public void insert(T t) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(createInsertQuery());
			buildObject(insertStatement, t);
			insertStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	/**
	 * Deletes all rows corresponding to the given condition (if field is primary key -> a single row deleted)
	 * @param field - name of field in WHERE clause
	 * @param id - value of field in WHERE clause
	 * @throws Exception
	 */
	public void delete(String field, Object id) throws Exception {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try {
			deleteStatement = dbConnection.prepareStatement(createDeleteQuery(field));
			deleteStatement.setObject(1, id);
			deleteStatement.execute();
		} catch (SQLException e) {
			//e.printStackTrace();
			throw new Exception("Cannot delete row");
		} catch (IllegalArgumentException e) {
			//e.printStackTrace();
			throw new Exception("Cannot delete row");
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
