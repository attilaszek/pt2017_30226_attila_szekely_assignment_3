package DAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import DAO.AbstractDAO;
import model.Client;

/**
 * 
 * @author Attila
 * Extends the AbstractDAO<T> class
 */

public class ClientDAO extends AbstractDAO<Client> {

	// uses basic CRUD methods from superclass

	// TODO: create only student specific queries
	
	@Override
	public void buildObject(PreparedStatement statement, Client client) {
		try {
			statement.setString(1, client.getCNP());
			statement.setString(2, client.getName());
			statement.setString(3, client.getTel());
			statement.setString(4, client.getEmail());
			statement.setString(5, client.getAddress());
			
			statement.setString(6, client.getCNP());
			statement.setString(7, client.getName());
			statement.setString(8, client.getTel());
			statement.setString(9, client.getEmail());
			statement.setString(10, client.getAddress());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}