package DAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import DAO.AbstractDAO;
import model.W_Order;

/**
 * 
 * @author Attila
 * Extends the AbstractDAO<T> class
 */

public class W_OrderDAO extends AbstractDAO<W_Order> {

	@Override
	public void buildObject(PreparedStatement statement, W_Order order) {
		try {
			statement.setInt(1, order.getId());
			statement.setString(2, order.getCNP_client());
			statement.setInt(3, order.getId_product());
			statement.setInt(4, order.getTotal());
			statement.setInt(5, order.getQuantity());
			statement.setDate(6, order.getDate());
			
			statement.setInt(7, order.getId());
			statement.setString(8, order.getCNP_client());
			statement.setInt(9, order.getId_product());
			statement.setInt(10, order.getTotal());
			statement.setInt(11, order.getQuantity());
			statement.setDate(12, order.getDate());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
