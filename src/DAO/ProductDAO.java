package DAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Product;

/**
 * 
 * @author Attila
 * Extends the AbstractDAO<T> class
 */

public class ProductDAO extends AbstractDAO<Product> {
	@Override
	public void buildObject(PreparedStatement statement, Product product) {
		try {
			statement.setInt(1, product.getId_product());
			statement.setString(2, product.getName());
			statement.setInt(3, product.getStock());
			statement.setInt(4, product.getPrice());
			
			statement.setInt(5, product.getId_product());
			statement.setString(6, product.getName());
			statement.setInt(7, product.getStock());
			statement.setInt(8, product.getPrice());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
