package presentation;

import java.util.List;

import model.Product;

/**
 * 
 * @author Attila
 * Form for insert and update Product table
 */

@SuppressWarnings("serial")
public class ProductForm extends AbstractForm<Product>{
	/**
	 * Constructor to add new Product
	 * @param window
	 */
	public ProductForm(AbstractWindow<Product> window) {
		super(window);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor to update an existing Product
	 * @param window
	 * @param listFieldValues
	 */
	public ProductForm(AbstractWindow<Product> window, List<String> listFieldValues) {
		super(window, listFieldValues);
		componentList.get(0).setEnabled(false);
		// TODO Auto-generated constructor stub
	}
}
