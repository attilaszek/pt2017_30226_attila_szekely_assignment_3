package presentation;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import businessLayer.ClientBLL;
import businessLayer.OrderBLL;
import businessLayer.ProductBLL;
import model.Client;
import model.Obj;
import model.Product;
import model.W_Order;

import java.lang.reflect.Field;

/**
 * 
 * @author Attila
 * General form for insert and update database tables
 * @param <T>
 */

@SuppressWarnings("serial")
public abstract class AbstractForm<T> extends JFrame {
	private final Class<T> type;
	private List<JLabel> labelList;
	protected List<JComponent> componentList;
	private JButton submitBtn;
	private JButton closeBtn;
	protected AbstractWindow<T> window;
	protected boolean update=false;
	
	/**
	 * Constructor, create and display class specific form (field names and input fields for values), init with empty values
	 * @param window
	 */
	@SuppressWarnings("unchecked")
	public AbstractForm(AbstractWindow<T> window) {
		this.window=window;
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		
		labelList = new ArrayList<JLabel>();
		componentList = new ArrayList<JComponent>();
		for (Field field : 	type.getDeclaredFields()) {
			labelList.add(new JLabel(field.getName().toUpperCase()));
			componentList.add(new JTextField());
		}
		
		GridLayout layout = new GridLayout(labelList.size() + 1, 2);
		this.setLayout(layout);
		layout.setHgap(20);
		layout.setVgap(30);
		
		for (int index=0; index<labelList.size(); index++) {
			this.add(labelList.get(index));
			this.add(componentList.get(index));
		}
		
		submitBtn = new JButton("Submit");
		closeBtn = new JButton("Close");
		this.add(submitBtn);
		this.add(closeBtn);
		
		this.setSize(300,300);
		this.setPreferredSize(new Dimension(300,60*(labelList.size()+1)));
	    this.setTitle("Table Example");    
	    this.pack();
	    this.setVisible(true);
	    
	    addListeners();
	}    
	
	/**
	 * Constructor, create and display class specific form (field names and input fields for values) -> update
	 * @param window
	 * @param listFieldValues -> list of values to be updated
	 */
	public AbstractForm(AbstractWindow<T> window, List<String> listFieldValues) {
		this(window);
		int index=0;
		for (Field field : 	type.getDeclaredFields()) {
			if (componentList.get(index) instanceof JTextField) {
				((JTextField)componentList.get(index)).setText(listFieldValues.get(index));
			}
			else {
				
			}
			index++;
		}
		update=true;
	}
	
	/**
	 * @return list of Strings with input values -> can be processed by the AbstractDAO to insert object in database
	 */
	private List<String> listValues() {
		List<String> list= new ArrayList<String>();
		for (JComponent comp : componentList) {
			if (comp instanceof JTextField) {
				list.add(((JTextField)comp).getText());
			}
			else {
				list.add(((Obj) (((JComboBox)comp).getSelectedItem())).getId().toString());
			}
		} 
		return list;
	}
	
	/**
	 * Add listeners for Submit (Save changes & exit) and Close (Discard changes & exit) buttons
	 */
	private void addListeners() {
		submitBtn.addActionListener(
				new ActionListener() {
             		@SuppressWarnings("unchecked")
					public void actionPerformed(ActionEvent e) {
                    	try {
                    		switch (window.getClass().getName()) {
                			case "presentation.ClientWindow":
                					new ClientBLL().insertClient(new Client(listValues()));
                					break;
                			case "presentation.ProductWindow":
                					new ProductBLL().insertProduct(new Product(listValues()));
									break;
                			case "presentation.W_OrderWindow": 
                					if (OrderBLL.decrementStock(
                							(Product)(((JComboBox)componentList.get(2)).getSelectedItem()), 
                							Integer.parseInt(listValues().get(4)))) {
                						new OrderBLL().insertOrder(new W_Order(listValues()));
                					}
									break;
                			default:	break;
                    		}
                    		
                    		setVisible(false);
                    		dispose();
                    		window.refreshTable(window.getObjectDAO().findAll());
                    	} catch (Exception ex) {
                    		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
                    	}
                    }
            	}
        );
    
        closeBtn.addActionListener(
            	new ActionListener() {
            		public void actionPerformed(ActionEvent e) {
                    	try {
                    		setVisible(false);
                    		dispose();
                    	} catch (Exception ex) {
                    		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
                    	}
                    }
            	}
        );   
	}
}
