package presentation;

import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import DAO.ClientDAO;
import DAO.ProductDAO;
import businessLayer.OrderBLL;
import model.Client;
import model.Product;
import model.W_Order;

/**
 * 
 * @author Attila
 * Form for insert and update Orders table
 */

@SuppressWarnings("serial")
public class W_OrderForm extends AbstractForm<W_Order>{
	/**
	 * Constructor for insert new Order
	 * @param window
	 */
	public W_OrderForm(AbstractWindow<W_Order> window) {
		super(window);
		classSpecificForm();
	}

	/**
	 * Constructor for update Client
	 * @param window
	 * @param listFieldValues
	 */
	public W_OrderForm(AbstractWindow<W_Order> window, List<String> listFieldValues) {
		super(window, listFieldValues);
		classSpecificForm();
	}
	
	/**
	 * Changing JTextField to JComboBoxes where field is foreign key and display names (not Id-s)
	 * <p>
	 * Making Total field uneditable
	 */
	private void classSpecificForm() {
		if (!update) {
			((JTextField) componentList.get(0)).setText("0");
		}
		componentList.get(0).setEnabled(false);
		componentList.get(3).setEnabled(false);
		
		this.remove(componentList.get(1));	
		JComboBox<Client> comboClient = new JComboBox<Client>();
		ClientDAO clientDAO = new ClientDAO();
		for (Client client : clientDAO.findAll()) {
			comboClient.addItem(client);
		}
		if (update) {
			comboClient.setSelectedItem((Client)(clientDAO.findByField("CNP", ((W_OrderWindow) window).getSelectedOrderCNP()).get(0)));
		}
		componentList.set(1, comboClient);
		this.add(componentList.get(1),3);	
		componentList.get(1).repaint();
		
		this.remove(componentList.get(2));
		JComboBox<Product> comboProduct = new JComboBox<Product>();
		ProductDAO productDAO = new ProductDAO();
		for (Product product : productDAO.findAll()) {
			comboProduct.addItem(product);
		}
		if (update) {
			comboProduct.setSelectedItem((Product)(productDAO.findByField("id_product", ((W_OrderWindow) window).getSelectedOrderId_product()).get(0)));
		}
		componentList.set(2, comboProduct);
		this.add(componentList.get(2),5);
		componentList.get(2).repaint();
		
		addListeners();
	}
	
	/**
	 * Adding keylistener to refresh order total when quantity changed
	 */
	private void addListeners() {
		componentList.get(4).addKeyListener(
				new KeyListener() {
					@Override
					public void keyPressed(java.awt.event.KeyEvent arg0) {
					}

					@Override
					public void keyReleased(java.awt.event.KeyEvent arg0) {
						try {
							((JTextField) componentList.get(3)).setText(Integer.toString(OrderBLL.orderTotal(
									Integer.parseInt(((JTextField) componentList.get(4)).getText()),
									((Product)((JComboBox) componentList.get(2)).getSelectedItem()).getId_product())));
						} catch (Exception e) {
							((JTextField) componentList.get(3)).setText("");
						}
					}

					@Override
					public void keyTyped(java.awt.event.KeyEvent arg0) {
					}
				});
	}
}
