package presentation;

import DAO.ProductDAO;
import model.Product;

/**
 * 
 * @author Attila
 * Window for displaying and manipulating Product table
 */

@SuppressWarnings("serial")
public class ProductWindow extends AbstractWindow<Product> {
	public ProductWindow() {
		super();
		setObjectDAO(new ProductDAO());
	}

	@Override
	public Object[] buildRow(Product product) {
		Object[] toReturn = new Object[Product.class.getDeclaredFields().length];
		toReturn[0] = product.getId_product();
		toReturn[1] = product.getName();
		toReturn[2] = product.getStock();
		toReturn[3] = product.getPrice();
		return toReturn;
	}
}
