package presentation;

import java.util.List;

import model.Client;

/**
 * 
 * @author Attila
 * Form for insert and update Client table
 */


@SuppressWarnings("serial")
public class ClientForm extends AbstractForm<Client> {
	/**
	 * Constructor to add new Client
	 * @param window
	 */
	public ClientForm(AbstractWindow<Client> window) {
		super(window);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor to update an existing Client
	 * @param window
	 * @param listFieldValues
	 */
	public ClientForm(AbstractWindow<Client> window, List<String> listFieldValues) {
		super(window, listFieldValues);
		componentList.get(0).setEnabled(false);
		// TODO Auto-generated constructor stub
	}
}
