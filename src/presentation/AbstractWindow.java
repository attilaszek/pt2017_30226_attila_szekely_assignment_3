package presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import DAO.AbstractDAO;

/**
 * 
 * @author Attila
 * General display window for a database entity, must be extended (implement BuildRow() method)
 * <p>
 * Displays a table with content of the database table (T), possible Delete, Update and Add operations
 * @param <T>
 */

@SuppressWarnings("serial")
public abstract class AbstractWindow<T> extends JFrame {
	private final Class<T> type;
	private AbstractDAO<T> objectDAO;
	protected List<T> objectList;
	protected JTable table;
	private DefaultTableModel model;
	private JButton delete, update, add;
	
	protected String[] columns; 
	
	/**
	 * Sets the field type to the actual type
	 */
	@SuppressWarnings("unchecked")
	public AbstractWindow() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	/**
	 * Creating and displaying the window
	 */
	public void initialize() {
		FlowLayout layout = new FlowLayout();
		this.setLayout(layout);
		
		JPanel panel = new JPanel();
		GridLayout panelLayout = new GridLayout(3,1);
		panelLayout.setVgap(30);
		panel.setLayout(panelLayout);
		delete = new JButton("Delete");
        panel.add(delete);
        update = new JButton("Update");
        panel.add(update);
        add = new JButton("Add");
        panel.add(add);
        this.add(panel);
        
        createTable(objectDAO.findAll());
        this.add(table);
        table.setRowSelectionAllowed(true); 
        this.add(new JScrollPane(table));
       
        this.setTitle("Table Example");      
        this.pack();
        this.setVisible(true);
	}
	
	/**
	 * Class specific build of a Row
	 * @param object - Object to be displayed in table
	 * @return an ObjectList with the field values of the given object (can be used as JTable rows)
	 */
	public abstract Object[] buildRow(T object);
	
	/**
	 * Creating the Table
	 * @param objectList
	 */
	public void createTable(List<T> objectList) {
		this.objectList=objectList;
		columns = new String[type.getDeclaredFields().length];
		int index=0;
		for (Field field : type.getDeclaredFields()) {
			columns[index] = field.getName();
			index++;
		}

        Object[][] data = new Object[objectList.size()][type.getDeclaredFields().length];
        index=0;
        for (T object : objectList) {
        	for (int innerIndex=0; innerIndex<type.getDeclaredFields().length; innerIndex++) {
    			data[index] = buildRow(object);
    		}
        	index++;
        }

        model = new DefaultTableModel(data, columns);
        table= new JTable();
        table.setModel(model);
	}
	
	/**
	 * Refreshing the table
	 * @param objectList
	 */
	public void refreshTable(List<T> objectList) {
		this.objectList=objectList;
		Object[][] data = new Object[objectList.size()][type.getDeclaredFields().length];
        int index=0;
        for (T object : objectList) {
        	for (int innerIndex=0; innerIndex<type.getDeclaredFields().length; innerIndex++) {
    			data[index] = buildRow(object);
    		}
        	index++;
        }
        model = new DefaultTableModel(data, columns);
        table.setModel(model);
        table.repaint();
	}
	
	/**
	 * Removing a row (from database & JTable)
	 * @param index - index of row to be deleted
	 */
	public void deleteByIndex(int index) {
		try {
			objectDAO.delete(type.getDeclaredFields()[0].getName(), table.getModel().getValueAt(index, 0));
			model.removeRow(index);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(new JFrame(), e.getMessage());
		}
	}
	
	/**
	 * @param index
	 * @return a list of Strings containing the values of a given row
	 */
	public List<String> getFieldValuesByIndex(int index) {
		List<String> fieldValues = new ArrayList<String>();
		for (int i=0; i<table.getModel().getColumnCount(); i++) {
			fieldValues.add(table.getModel().getValueAt(index, i).toString());
		}
		return fieldValues;
	}
	
	public void addDeleteListener(ActionListener del) {
	    delete.addActionListener(del);
	}
	
	public void addUpdateListener(ActionListener upd) {
		update.addActionListener(upd);
	}
	
	public void addAddListener(ActionListener add) {
		this.add.addActionListener(add);
	}
	
	public int getTableIndex() {
		return table.getSelectedRow();
	}
	
	public AbstractDAO<T> getObjectDAO() {
		return objectDAO;
	}
	
	public void setObjectDAO(AbstractDAO<T> objectDAO) {
		this.objectDAO=objectDAO;;
	}
}
