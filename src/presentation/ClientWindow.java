package presentation;

import DAO.ClientDAO;
import model.Client;

/**
 * 
 * @author Attila
 * Window for displaying and manipulating Client table
 */

@SuppressWarnings("serial")
public class ClientWindow extends AbstractWindow<Client>{
	public ClientWindow() {
		super();
		setObjectDAO(new ClientDAO());
	}

	@Override
	public Object[] buildRow(Client client) {
		Object[] toReturn = new Object[Client.class.getDeclaredFields().length];
		toReturn[0] = client.getCNP();
		toReturn[1] = client.getName();
		toReturn[2] = client.getTel();
		toReturn[3] = client.getEmail();
		toReturn[4] = client.getAddress();
		return toReturn;
	}

	
	
}
