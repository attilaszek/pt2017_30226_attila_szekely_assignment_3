package presentation;

import DAO.ClientDAO;
import DAO.ProductDAO;
import DAO.W_OrderDAO;
import model.W_Order;

/**
 * 
 * @author Attila
 * Window for display and manipulate order table, implements methods from abstract class AbstractWindow<T>
 */

public class W_OrderWindow extends AbstractWindow<W_Order> {
	public W_OrderWindow() {
		super();
		setObjectDAO(new W_OrderDAO());
	}

	@Override
	public Object[] buildRow(W_Order order) {
		Object[] toReturn = new Object[W_Order.class.getDeclaredFields().length];
		toReturn[0] = order.getId();
		
		columns[1]="Client name";
		ClientDAO clientDAO = new ClientDAO();
		toReturn[1] = clientDAO.findByField("CNP", order.getCNP_client()).get(0).getName();
		
		columns[2]="Product";
		ProductDAO productDAO = new ProductDAO();
		toReturn[2] = productDAO.findByField("id_product", order.getId_product()).get(0).getName();
		
		toReturn[3] = order.getTotal();
		toReturn[4] = order.getQuantity();
		toReturn[5] = order.getDate();
		return toReturn;
	}
	
	public String getSelectedOrderCNP() {
		return objectList.get(table.getSelectedRow()).getCNP_client();
	}
	
	public int getSelectedOrderId_product() {
		return objectList.get(table.getSelectedRow()).getId_product();
	}
}
