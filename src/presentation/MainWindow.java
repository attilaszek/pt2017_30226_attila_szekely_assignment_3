package presentation;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.Controller;
import model.Client;
import model.Product;
import model.W_Order;


/**
 * 
 * @author Attila
 * <p>
 * Creates the main interface by extending the JFrame class.
 * <p>
 * Contains 3 buttons, corresponding to the 3 entities: client, product and order.
 * When this window is closed, the program exits.
 */
public class MainWindow extends JFrame {
	private JButton clientBtn, productBtn, orderBtn;
	/**
	 * Contains 3 buttons and adds for each button their listeners, which call the constructors of the 3 
	 * class specific windows.
	 */
	public MainWindow() {
		FlowLayout layout = new FlowLayout();
		layout.setVgap(40);
		layout.setHgap(300);
		this.setLayout(layout);
		this.setPreferredSize(new Dimension(500,300));
		this.setLocation(300,200);
		
		
		clientBtn = new JButton("Clients");
		productBtn = new JButton("Products");
		orderBtn = new JButton("Orders");
		
		this.add(clientBtn);
		this.add(productBtn);
		this.add(orderBtn);
		
        this.setTitle("Table Example");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
        this.pack();
        this.setVisible(true);
        
        clientBtn.addActionListener(
            	new ActionListener() {
            		public void actionPerformed(ActionEvent e) {
                    	try {
                    		ClientWindow clientWindow = new ClientWindow();
                    		clientWindow.initialize();
                    		Controller<Client> controller = new Controller<Client>(clientWindow);
                    	} catch (Exception ex) {
                    		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
                    	}
                    }
            	}
        );
        
        productBtn.addActionListener(
            	new ActionListener() {
            		public void actionPerformed(ActionEvent e) {
                    	try {
                    		ProductWindow productWindow = new ProductWindow();
                    		productWindow.initialize();
                    		Controller<Product> controller2 = new Controller<Product>(productWindow);
                    	} catch (Exception ex) {
                    		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
                    	}
                    }
            	}
        );
        
        orderBtn.addActionListener(
            	new ActionListener() {
            		public void actionPerformed(ActionEvent e) {
                    	try {
                    		W_OrderWindow orderWindow = new W_OrderWindow();
                    		orderWindow.initialize();
                    		Controller<W_Order> controller3 = new Controller<W_Order>(orderWindow);
                    	} catch (Exception ex) {
                    		JOptionPane.showMessageDialog(new JFrame(), ex.getMessage());
                    	}
                    }
            	}
        );
	}
	
	/**
	 * Entry point in the application
	 * Calls the MainWindow constructor
	 */
	public static void main(String args[]) {
		MainWindow main = new MainWindow();
	}
}
