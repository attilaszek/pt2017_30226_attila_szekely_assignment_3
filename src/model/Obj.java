package model;

/**
 * 
 * @author Attila
 * <p>
 * A common interface for the database items
 */

public interface Obj {
	/**
	 * Must be implemented in database item classes
	 * <p>
	 * Makes easier to find ID's of database objects (CNP, id_order, ...), no need for cast to Product, Client, etc.
	 * @return the ID that corresponds to the given object
	 */
	public Object getId();
}
