package model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 
 * @author Attila
 * <p>
 * Contains the fields from the database W_Order table
 * <p>
 * Contains getters and setters of its fields
 * <p>
 * Implements the Obj interface
 */

public class W_Order {
	private int id;
	private String CNP_client;
	private int id_product;
	private int total;
	private int quantity;
	private Date date;
	
	/**
	 * Empty constructor
	 */
	public W_Order() {
	}
	
	/**
	 * Constructor, uses field values to instantiate a W_Order object
	 * @param id
	 * @param CNP_client
	 * @param id_product
	 * @param total
	 * @param quantity
	 * @param date
	 */
	public W_Order(int id, String CNP_client, int id_product, int total, int quantity, Date date) {
		super();
		this.id = id;
		this.CNP_client = CNP_client;
		this.id_product = id_product;
		this.total = total;
		this.quantity = quantity;
		this.date = date;
	}
	
	/**
	 * Constructor, get a list of Strings, containing the initialization values
	 * <p>
	 * Useful when getting values from a form (with JTextField components)
	 * <p>
	 * Date format at input (String index:5): yyyy-MM-dd
	 * @param listValues
	 */
	public W_Order(List<String> listValues) {
		super();
		
		this.id = Integer.parseInt(listValues.get(0));
		this.CNP_client = listValues.get(1);
		this.id_product = Integer.parseInt(listValues.get(2));
		this.total = Integer.parseInt(listValues.get(3));
		this.quantity = Integer.parseInt(listValues.get(4));
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd"); 
		try {
			this.date = new Date(simpleDateFormat.parse(listValues.get(5)).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id_order) {
		this.id = id_order;
	}
	public String getCNP_client() {
		return CNP_client;
	}
	public void setCNP_client(String cNP_client) {
		CNP_client = cNP_client;
	}
	public int getId_product() {
		return id_product;
	}
	public void setId_product(int id_product) {
		this.id_product = id_product;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
