package model;

import java.util.List;

/**
 * 
 * @author Attila
 * <p>
 * Contains the fields from the database Client table
 * <p>
 * Contains getters and setters of its fields
 * <p>
 * Implements the Obj interface
 */

public class Client implements Obj{
	private String CNP;
	private String name;
	private String tel;
	private String email;
	private String address;
	
	/**
	 * Empty constructor
	 */
	public Client() {
	}
	
	/**
	 * Constructor, uses field values to instantiate a Client object
	 * @param CNP
	 * @param name
	 * @param tel
	 * @param email
	 * @param address
	 */
	public Client(String CNP, String name, String tel, String email, String address) {
		super();
		this.CNP = CNP;
		this.name = name;
		this.tel = tel;
		this.email = email;
		this.address = address;
	}
	
	/**
	 * Constructor, get a list of Strings, containing the initialization values
	 * <p>
	 * Useful when getting values from a form (with JTextField components)
	 * @param listValues - list of Strings, must be cast to class specific types: String, Integer, etc.
	 */
	public Client(List<String> listValues) {
		super();
		this.CNP = listValues.get(0);
		this.name = listValues.get(1);
		this.tel = listValues.get(2);
		this.email = listValues.get(3);
		this.address = listValues.get(4);
	}

	public String getCNP() {
		return CNP;
	}

	public void setCNP(String cNP) {
		CNP = cNP;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toString() {
		return name+" ("+CNP+")";
	}
	
	/**
	 * Overrides the equals() method:
	 * Two client are "equal" if their CNP is the same (CNP is an unique identifier in the client table)
	 * @return true (equal), false (not equal)
	 */
	public boolean equals(Object other) {
		if (this.CNP.equals(((Client)other).getCNP())) {
			return true;
		}
		return false;
	}

	/**
	 * Implements getId() form Obj interface
	 * @return CNP
	 */
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return CNP;
	}
}
