package model;

import java.util.List;

/**
 * 
 * @author Attila
 * <p>
 * Contains the fields from the database Product table
 * <p>
 * Contains getters and setters of its fields
 * <p>
 * Implements the Obj interface
 */

public class Product implements Obj {
	private int id_product;
	private String name;
	private int stock;
	private int price;
	
	/**
	 * Empty constructor
	 */
	public Product() {
	}
	
	/**
	 * Constructor, uses field values to instantiate a Product object
	 * @param id_product
	 * @param name
	 * @param stock
	 * @param price
	 */
	public Product(int id_product, String name, int stock, int price) {
		super();
		this.id_product = id_product;
		this.name = name;
		this.stock = stock;
		this.price = price;
	}
	
	/**
	 * Constructor, get a list of Strings, containing the initialization values
	 * <p>
	 * Useful when getting values from a form (with JTextField components)
	 * @param listValues - list of Strings, must be cast to class specific types: String, Integer, etc.
	 */
	public Product(List<String> listValues) {
		super();
		this.id_product = Integer.parseInt(listValues.get(0));
		this.name = listValues.get(1);
		this.stock = Integer.parseInt(listValues.get(2));
		this.price = Integer.parseInt(listValues.get(3));
	}

	public int getId_product() {
		return id_product;
	}

	public void setId_product(int id_product) {
		this.id_product = id_product;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public String toString() {
		return name+" ("+id_product+")";
	}
	
	/**
	 * Overrides the equals() method:
	 * Two client are "equal" if their id_product is the same (id_product is an unique identifier in the client table)
	 * @return true (equal), false (not equal)
	 */
	public boolean equals(Object other) {
		if (this.id_product == ((Product)other).getId_product()) {
			return true;
		}
		return false;
	}

	/**
	 * Implements getId() form Obj interface
	 * @return id_product
	 */
	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id_product;
	}
}
