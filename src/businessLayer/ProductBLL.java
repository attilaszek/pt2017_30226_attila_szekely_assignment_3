package businessLayer;

import java.util.ArrayList;
import java.util.List;

import DAO.ProductDAO;
import businessLayer.validators.Validator;
import model.Product;

/**
 * 
 * @author Attila
 * Business logic for products
 */

public class ProductBLL {

	private List<Validator<Product>> validators;
	
	/**
	 * Initialize validator list
	 */
	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
	}
	
	/**
	 * Insert a product
	 * @param product
	 */
	public void insertProduct(Product product) {
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
		new ProductDAO().insert(product);
	}
	
	/**
	 * Find a product by id_product
	 * @param Id - id of product to find
	 * @return a Product
	 */
	public Product findProductById(int Id) {
		return new ProductDAO().findByField("id_product", Id).get(0);
	}
}
