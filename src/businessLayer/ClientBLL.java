package businessLayer;

import java.util.ArrayList;
import java.util.List;

import DAO.ClientDAO;
import businessLayer.validators.EmailValidator;
import businessLayer.validators.Validator;
import model.Client;

/**
 * 
 * @author Attila
 * Business logic for Clients
 */

public class ClientBLL {

	private List<Validator<Client>> validators;
	
	/**
	 * Constructor, initialize validators
	 */
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
	}
	
	/**
	 * Inserting client with validation
	 * @param client - client to be inserted
	 */
	public void insertClient(Client client) {
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		new ClientDAO().insert(client);
	}
	
	/**
	 * Find client by CNP
	 * @param CNP - CNP of client
	 * @return a Client
	 */
	public Client fincClientByCNP(String CNP) {
		return new ClientDAO().findByField("CNP", CNP).get(0);
	}
}
