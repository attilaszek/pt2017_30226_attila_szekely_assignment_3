package businessLayer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import DAO.ProductDAO;
import DAO.W_OrderDAO;
import businessLayer.validators.Validator;
import model.W_Order;
import model.Product;

/**
 * 
 * @author Attila
 * Business logic for orders
 */

public class OrderBLL {
	
	private List<Validator<W_Order>> validators;
	
	/**
	 * initialize validators list
	 */
	public OrderBLL() {
		validators = new ArrayList<Validator<W_Order>>();
	}
	
	/**
	 * Calculating total price of an order
	 * @param quantity 
	 * @param id_product
	 * @return total price
	 */
	public static int orderTotal(int quantity, int id_product) {
		return quantity * (new ProductDAO()).findByField("id_product", id_product).get(0).getPrice();
	}
	
	/**
	 * Decrementing stock if possible, else returning false (The caller must threat it)
	 * @param product - product to decrement stock
	 * @param quantity - nr. of products ordered
	 * @return true if successfully decrement, false if understock
	 */
	public static boolean decrementStock(Product product, int quantity) {
		if (product.getStock() >= quantity) {
			product.setStock(product.getStock() - quantity);
			new ProductDAO().insert(product);
			return true;
		}
		return false;
	}
	
	/**
	 * Inserting an order
	 * @param order
	 */
	public void insertOrder(W_Order order) {
		for (Validator<W_Order> v : validators) {
			v.validate(order);
		}
		new W_OrderDAO().insert(order);
		
		createBill(order);
	}
	
	/**
	 * Creating a bill in .txt format
	 * @param order - order to be printed
	 */
	public void createBill(W_Order order) {
		model.Client client = new ClientBLL().fincClientByCNP(order.getCNP_client());
		model.Product product = new ProductBLL().findProductById(order.getId_product());
		List<W_Order> listOrders = new W_OrderDAO().findAll();
		order = listOrders.get(listOrders.size()-1);
		try{
			String fileName= "order_no_"+Integer.toString(order.getId())+".txt";
		    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		    writer.println("Client data:");
		    writer.println("    Name: "+client.getName());
		    writer.println("    CNP: "+client.getCNP());
		    writer.println("    Tel: "+client.getTel());
		    writer.println("    Email: "+client.getEmail());
		    writer.println("    Address: "+client.getAddress());
		    writer.println("Order no_"+Integer.toString(order.getId()));
		    writer.println("    Product: "+product.getName());
		    writer.println("    Price: "+product.getPrice());
		    writer.println("    Quantity: "+order.getQuantity());
		    writer.println("    Total price: "+order.getTotal());
		    writer.println("    Date: "+order.getDate());
		    writer.close();
		} catch (IOException e) {
		   // do something
		}
	}
}
